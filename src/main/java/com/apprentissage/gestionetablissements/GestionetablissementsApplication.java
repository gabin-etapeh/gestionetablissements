package com.apprentissage.gestionetablissements;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionetablissementsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionetablissementsApplication.class, args);
	}

}
