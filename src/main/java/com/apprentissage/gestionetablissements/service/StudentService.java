package com.apprentissage.gestionetablissements.service;

import com.apprentissage.gestionetablissements.entity.Student;
import com.apprentissage.gestionetablissements.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;

    public StudentService (StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    public Student addStudent (Student student){
        return studentRepository.save(student);
    }

    public Student getStudent (Integer id){
        return studentRepository.findById(id).orElse(null);
    }

    public Optional<Student> updateStudent (Integer id, Student nouveauStudent){
        Optional<Student> ancienStudentOptional = studentRepository.findById(id);
        if(ancienStudentOptional.isPresent()){

            Student ancienStudent = (Student) ancienStudentOptional.get();
            ancienStudent.setNom(nouveauStudent.getNom());
            ancienStudent.setPrenom(nouveauStudent.getPrenom());

            return Optional.of(studentRepository.save(ancienStudent));
        } else {
            return Optional.empty();
        }
    }

    public void deleteStudent(Integer id) {
        studentRepository.deleteById(id);

    }
}
