package com.apprentissage.gestionetablissements.controller;

import com.apprentissage.gestionetablissements.entity.Student;
import com.apprentissage.gestionetablissements.service.StudentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/")
    public String index() {
        return "Welcome on my school App !";
    }

    @GetMapping("/hello")
    public String hello() {
        return "Welcome on my app ";
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<Student> getStudent (@PathVariable Integer id){
        return new ResponseEntity<>(studentService.getStudent(id), HttpStatus.OK);
    }
    @PostMapping("/student")
    public ResponseEntity<Student> addStudent (@RequestBody Student student) {
        return new ResponseEntity<>(studentService.addStudent(student), HttpStatus.CREATED);
    }
    @PutMapping("/student/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable Integer id,
                                          @RequestBody Student nouveauStudent){
        Optional<Student> updatedStudent = studentService.updateStudent(id, nouveauStudent);
        if(updatedStudent.isPresent()){
            return new ResponseEntity<>(updatedStudent.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Aucun eleve à modifier", HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/student/{id}")
    public ResponseEntity<Student> deleteStudent (@PathVariable Integer id){
        studentService.deleteStudent(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
