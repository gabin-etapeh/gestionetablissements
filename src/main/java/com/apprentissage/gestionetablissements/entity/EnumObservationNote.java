package com.apprentissage.gestionetablissements.entity;

public enum EnumObservationNote {
    MAL("Malade"),
    ABS("Absent");

    private final String value;

    public String getValue() {
        return value;
    }

    EnumObservationNote(String value) {
        this.value = value;
    }
}
