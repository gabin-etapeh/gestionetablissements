package com.apprentissage.gestionetablissements.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Programmation {

    @EmbeddedId
    private IdProgrammation idProgrammation;
    private Float coefficient;
    private LocalDate date;
}
