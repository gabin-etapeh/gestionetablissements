package com.apprentissage.gestionetablissements.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Eleve {

    @Id
    private String matricule;

    private String nom;
    private String prenom;
    private LocalDate dateNaissance;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Classe classe;

}
