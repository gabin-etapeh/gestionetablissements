package com.apprentissage.gestionetablissements.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Note {

    private IdNote idNote;

    private Float mark;
    private LocalDate date;
    private String appreciation;

    @Enumerated(EnumType.STRING)
    private EnumObservationNote observation;
}
