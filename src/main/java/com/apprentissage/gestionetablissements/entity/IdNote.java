package com.apprentissage.gestionetablissements.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor

public class IdNote {
    @ManyToOne
    private  Eleve eleve;
    @ManyToOne
    private  Programmation programmation;
    @ManyToOne
    private  Evaluation evaluation;
}
